package com.ole.weatherforecast;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WeatherActivity extends AppCompatActivity {

    private static final String UNIT = "metric";
    private static final String API_KEY = ""; // Insert your API_KEY

    private List<Title> listDataHeader;
    private HashMap<Title, List<WeatherModel.Listok>> listHash;

    private ExpandableListView listView;
    private ExpandableListViewAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        String cityName = intent.getStringExtra("SearchCity");
        Double longitude = intent.getDoubleExtra("MyCoordinateX", 0);
        Double latitude = intent.getDoubleExtra("MyCoordinateY", 0);

        if (longitude == 0 && latitude == 0) {
            getWeatherFromServer(cityName, UNIT, API_KEY);
        } else {
            getWeatherFromServerMyLocation(latitude, longitude, UNIT, API_KEY);
        }
        listView = (ExpandableListView) findViewById(R.id.lvExp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private String convertToDayOfWeek(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dayWeek = null;
        try {
            dayWeek = format.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("EEEE").format(dayWeek);
    }

    private void putResponseToExpandableList(List<WeatherModel.Listok> responseList) {
        List<WeatherModel.Listok> dayOne = new ArrayList<WeatherModel.Listok>();
        List<WeatherModel.Listok> dayTwo = new ArrayList<WeatherModel.Listok>();
        List<WeatherModel.Listok> dayThree = new ArrayList<WeatherModel.Listok>();
        List<WeatherModel.Listok> dayFour = new ArrayList<WeatherModel.Listok>();
        List<WeatherModel.Listok> dayFive = new ArrayList<WeatherModel.Listok>();
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();
        int temp = 0;
        String dateTitle = "";
        for (int j = 0; j < 5; j++) {
            String str = responseList.get(temp).getDateTxt().substring(8, 10);
            for (int i = temp; temp < responseList.size(); i++) {
                if (i < responseList.size() && responseList.get(i).getDateTxt().substring(8, 10).equals(str)) {
                    switch (j) {
                        case 0:
                            dayOne.add(responseList.get(i));
                            break;
                        case 1:
                            dayTwo.add(responseList.get(i));
                            break;
                        case 2:
                            dayThree.add(responseList.get(i));
                            break;
                        case 3:
                            dayFour.add(responseList.get(i));
                            break;
                        case 4:
                            dayFive.add(responseList.get(i));
                            break;
                    }
                } else {
                    dateTitle = responseList.get(temp).getDateTxt().substring(0, 10);
                    listDataHeader.add(new Title(convertToDayOfWeek(dateTitle), dateTitle));
                    temp = i;
                    break;
                }
            }
            switch (j) {
                case 0:
                    listHash.put(listDataHeader.get(j), dayOne);
                    break;
                case 1:
                    listHash.put(listDataHeader.get(j), dayTwo);
                    break;
                case 2:
                    listHash.put(listDataHeader.get(j), dayThree);
                    break;
                case 3:
                    listHash.put(listDataHeader.get(j), dayFour);
                    break;
                case 4:
                    listHash.put(listDataHeader.get(j), dayFive);
                    break;
            }
        }
    }

    private void getWeatherFromServer(String therm, String units, String apiKey) {
        App.getApi().getWeather(therm, units, apiKey).
                enqueue(new Callback<WeatherModel>() {
                    @Override
                    public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            setTitle(response.body().getCity().getName()
                                    + ", " + response.body().getCity().getCountry());
                            putResponseToExpandableList(response.body().getListok());
                            listAdapter = new ExpandableListViewAdapter(getApplication(), listDataHeader, listHash);
                            listView.setAdapter(listAdapter);
                        } else {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            Toast.makeText(WeatherActivity.this, "Incorrect city", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherModel> call, Throwable t) {
                        Log.d("Error FromServer", t.getMessage());
                    }
                });
    }

    private void getWeatherFromServerMyLocation(Double latitude, Double longitude, String units, String apiKey) {
        App.getApi().getWeatherMyLocation(latitude, longitude, units, apiKey).
                enqueue(new Callback<WeatherModel>() {
                    @Override
                    public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            if (response.body().getCity().getName() == null) {
                                setTitle("unidentified");
                            } else {
                                setTitle(response.body().getCity().getName()
                                        + ", " + response.body().getCity().getCountry());
                            }
                            putResponseToExpandableList(response.body().getListok());
                            listAdapter = new ExpandableListViewAdapter(getApplication(), listDataHeader, listHash);
                            listView.setAdapter(listAdapter);
                        } else {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherModel> call, Throwable t) {
                        Log.d("Error ServerMyLocation", t.getMessage());
                    }
                });
    }


}
