package com.ole.weatherforecast;


public class Title {

    String day;
    String date;

    public String getDay() {
        return day;
    }

    public String getDate() {
        return date;
    }

    public Title(String day, String date) {
        this.day = day;
        this.date = date;
    }
}
