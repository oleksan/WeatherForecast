package com.ole.weatherforecast;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherModel {
    @SerializedName("list")
    @Expose
    List<Listok> listok;

    @SerializedName("city")
    @Expose
    City city;


    public List<Listok> getListok() {
        return listok;
    }

    public City getCity() {
        return city;
    }

    class City {
        @SerializedName("name")
        @Expose
        String name;

        @SerializedName("country")
        @Expose
        String country;

        public String getName() {
            return name;
        }

        public String getCountry() {
            return country;
        }
    }

    public static class Listok {

        @SerializedName("weather")
        @Expose
        private List<Weather> weathers;
        @SerializedName("main")
        @Expose
        private Main main;

        public Main getMain() {
            return main;
        }

        @SerializedName("dt")
        @Expose
        private Long dt;
        @SerializedName("wind")
        @Expose
        private Wind wind;

        public Wind getWind() {
            return wind;
        }

        public class Wind {
            @SerializedName("speed")
            @Expose
            private Double speed;

            public Double getSpeed() {
                return speed;
            }
        }

        public Long getDt() {
            return dt;
        }

        public String getDateTxt() {
            return dateTxt;
        }

        @SerializedName("dt_txt")
        @Expose
        private String dateTxt;

        public List<Weather> getWeathers() {
            return weathers;
        }

    }

    public static class Weather {

        private String main;

        private String description;

        private String icon;

        public String getMain() {
            return main;
        }

        public String getDescription() {
            return description;
        }

        public String getIcon() {
            return icon;
        }
    }

    public static class Main {

        @SerializedName("temp")
        @Expose
        private Double temp;

        @SerializedName("humidity")
        @Expose
        private Integer humidity;

        private Double temp_min;

        private Double temp_max;


        public Double getTemp() {
            return temp;
        }

        public Integer getHumidity() {
            return humidity;
        }

        public Double getTemp_min() {
            return temp_min;
        }

        public Double getTemp_max() {
            return temp_max;
        }
    }
}


