package com.ole.weatherforecast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.List;


public class ExpandableListViewAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<Title> listDataHeader;
    private HashMap<Title,List<WeatherModel.Listok>> listHashMap;

    public ExpandableListViewAdapter(Context context,
                                     List<Title> listDataHeader,
                                     HashMap<Title, List<WeatherModel.Listok>> listHashMap) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listHashMap = listHashMap;
    }

    @Override
    public int getGroupCount() {
        if (listDataHeader == null)
            return 0;
        return listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listHashMap.get(listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(listDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Title headerTitle = (Title)getGroup(groupPosition);
        if(convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_card_weather_parent, null);
        }
        TextView tvDayOfWeek = (TextView)convertView.findViewById(R.id.tvDayOfWeek);
        TextView tvDate = (TextView)convertView.findViewById(R.id.tvDate);
        tvDayOfWeek.setText(headerTitle.getDay());
        tvDate.setText(headerTitle.getDate());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final WeatherModel.Listok childObj = (WeatherModel.Listok)getChild(groupPosition, childPosition);
        if(convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_card_weather_child, null);
        }
        TextView tvTemperature = (TextView)convertView.findViewById(R.id.tvTemperature);
        TextView tvHour = (TextView)convertView.findViewById(R.id.tvHour);
        TextView tvHumidity = (TextView)convertView.findViewById(R.id.tvHumidity);
        TextView tvDescription = (TextView)convertView.findViewById(R.id.tvDescription);
        TextView tvTemperatureSign = (TextView)convertView.findViewById(R.id.tvTemperatureSign);
        TextView tvWind = (TextView)convertView.findViewById(R.id.tvWind);
        ImageView weatherImage = (ImageView)convertView.findViewById(R.id.weather_image);

        tvTemperature.setText(Math.round(childObj.getMain().getTemp()) + "");
        tvDescription.setText(childObj.getWeathers().get(0).getDescription());
        tvHumidity.setText(childObj.getMain().getHumidity() + "%");
        tvHour.setText(childObj.getDateTxt().substring(11, 16));
        tvWind.setText(Math.round(childObj.getWind().getSpeed()) + "m/s");
        if (childObj.getMain().getTemp() > 0){
            tvTemperatureSign.setText("+");
        }

        Glide.with(context)
                .load("http://openweathermap.org/img/w/" + childObj.getWeathers().get(0).getIcon() + ".png" )
                .thumbnail(0.5f)
                .into(weatherImage);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
