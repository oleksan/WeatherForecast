package com.ole.weatherforecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiEndPoint {

    @GET("data/2.5/forecast")
    Call<WeatherModel> getWeather(@Query("q") String query,
                                  @Query("units") String units,
                                  @Query("appid") String apiKey);

    @GET("data/2.5/forecast")
    Call<WeatherModel> getWeatherMyLocation(@Query("lat") Double latitude ,
                                            @Query("lon") Double longitude,
                                            @Query("units") String units,
                                            @Query("appid") String apiKey);

}
